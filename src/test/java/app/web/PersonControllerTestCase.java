package app.web;

import app.domain.Person;
import app.utils.AbstractTest;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DatabaseSetup(value = "person/person_controller_create_before.xml")
@DatabaseTearDown(value = "person/person_controller_create_before.xml", type = DatabaseOperation.DELETE_ALL)
public class PersonControllerTestCase extends AbstractTest {

    private ObjectWriter jsonWriter;

    @Before
    public void setUp(){
        this.jsonWriter = this.json().writerWithView(Person.class);

    }


    @Test
    @ExpectedDatabase(value = "person/person_controller_create_after.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void createPersonShouldReturn200WhenPersonIsOk() throws Exception{

        //Arrange
        Person person = new Person();
        person.setName("Joao da Silva");
        person.setBirthDate("1883-03-24");
        person.setEmail("joaosilva@mailnator.com");

        //Act and Assert
        this.mvc().perform(post("/person")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(this.jsonWriter.writeValueAsString(person)))
                .andExpect(status().isOk())
                .andDo(print());

    }

    @Test
    public void createPersonShouldReturn422WhenPersonNameIsNull() throws Exception{

        //Arrange
        Person person = new Person();
        person.setBirthDate("1883-03-24");
        person.setEmail("joaosilva@mailnator.com");
        person.setName(null);

        //Act and Assert
        this.mvc().perform(post("/person")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.jsonWriter.writeValueAsString(person)))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$[0]").value(this.messages("person.name.cannot.be.null.or.empty")));

    }

    @Test
    public void getPersonByNameWhereNameIsJon() throws Exception{

        //Act and Assert
        this.mvc().perform(get("/person/name/{name}", "Jon")
                    .param("name", "Jon"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("[0].name").value("Jon Doe"))
                .andExpect(jsonPath("[0].email").value("jondoe@mailnator.com"));

    }

    @Test
    public void getPersonByNameShouldReturnExceptionWhenNameIsBlank() throws Exception{

        //Act and Assert
        this.mvc().perform(get("/person/name/{name}", "")
                .param("name", ""))
                .andDo(print())
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void getPersonByNameWhereNameContainsDoe() throws Exception{

        //Act and Assert
        this.mvc().perform(get("/person/name/{name}", "Doe")
                .param("name", "Doe"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("[0].name").value("Jane Doe"))
                .andExpect(jsonPath("[0].email").value("janedoe@mailnator.com"))
                .andExpect(jsonPath("[1].name").value("Jon Doe"))
                .andExpect(jsonPath("[1].email").value("jondoe@mailnator.com"));

    }

    @Test
    @DatabaseSetup(value = "person/person_controller_get_before_with_id.xml")
    public void getPersonByIdWhereIdIs1() throws Exception{

        //Act and Assert
        this.mvc().perform(get("/person/id/{id}", 1)
                .param("id", "1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("name").value("Jane Doe"))
                .andExpect(jsonPath("email").value("janedoe@mailnator.com"))
                .andExpect(jsonPath("birthDate").value("1989-06-15"));

    }

    @Test
    @DatabaseSetup(value = "person/person_controller_get_before_with_id.xml")
    @ExpectedDatabase(value = "person/person_controller_update_after_with_id.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void updatePersonWhereIdIs1() throws Exception{

        //Arrange
        Person person = new Person();
        person.setId(1);
        person.setName("Jane Doe");
        person.setBirthDate("1883-03-24");
        person.setEmail("janedoe@mailnator.com");

        //Act and Assert
        this.mvc().perform(put("/person")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.jsonWriter.writeValueAsString(person)))
                .andExpect(status().isOk())
                .andDo(print());

    }

    @Test
    public void updatePersonShouldReturn422WhenPersonNameIsBlank() throws Exception{

        //Arrange
        Person person = new Person();
        person.setId(1);
        person.setName("");
        person.setBirthDate("1883-03-24");
        person.setEmail("janedoe@mailnator.com");

        //Act and Assert
        this.mvc().perform(put("/person")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.jsonWriter.writeValueAsString(person)))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$[0]").value(this.messages("person.name.cannot.be.null.or.empty")));

    }

    @Test
    @DatabaseSetup(value = "person/person_controller_get_before_with_id.xml")
    @ExpectedDatabase(value = "person/person_controller_delete_after_with_id.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void deletePersonWhereIdIs1() throws Exception{

        //Arrange
        Person person = new Person();
        person.setId(1);
        person.setName("Jane Doe");
        person.setBirthDate("1989-06-15");
        person.setEmail("janedoe@mailnator.com");

        //Act and Assert
        this.mvc().perform(delete("/person")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.jsonWriter.writeValueAsString(person)))
                .andExpect(status().isOk())
                .andDo(print());

    }

    @Test
    public void deletePersonShouldReturn400WhenIdDoesNotExists() throws Exception{

        //Arrange
        Person person = new Person();
        person.setId(999);
        person.setName("Jane Doe");
        person.setBirthDate("1989-06-15");
        person.setEmail("janedoe@mailnator.com");

        //Act and Assert
        this.mvc().perform(delete("/person")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.jsonWriter.writeValueAsString(person)))
                .andDo(print())
                .andExpect(status().isBadRequest());

    }

}
