package app.web;


import app.domain.Person;
import app.domain.ProfessionalExperience;
import app.utils.AbstractTest;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DatabaseSetup(value = "proexp/proexp_controller_before.xml")
@DatabaseTearDown(value = "proexp/proexp_controller_before.xml", type = DatabaseOperation.DELETE_ALL)
public class ProfessionalExperienceTestCase extends AbstractTest{

    private ObjectWriter jsonWriter;
    private Person person;

    @Before
    public void setUp(){
        this.jsonWriter = this.json().writerWithView(ProfessionalExperience.class);
        person = new Person();
        person.setId(1);
        person.setName("Jane Doe");
        person.setEmail("janedoe@mailnator.com");
        person.setBirthDate("1989-06-15");

    }

    @Test
    @ExpectedDatabase(value = "proexp/proexp_controller_create_after.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void createProExpShouldReturn200WhenProExpIsOk() throws Exception{

        //Arrange
        ProfessionalExperience proExp = new ProfessionalExperience();
        proExp.setCompany("Google");
        proExp.setActivities("Developer");
        proExp.setPerson(person);

        //Act and Assert
        this.mvc().perform(post("/professionalexperience")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.jsonWriter.writeValueAsString(proExp)))
                .andExpect(status().isOk())
                .andDo(print());

    }

    @Test
    public void createProExpShouldReturn422WhenCompanyIsNull() throws Exception{

        //Arrange
        ProfessionalExperience proExp = new ProfessionalExperience();
        proExp.setCompany(null);
        proExp.setActivities("Developer");
        proExp.setPerson(person);

        //Act and Assert
        this.mvc().perform(post("/professionalexperience")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.jsonWriter.writeValueAsString(proExp)))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$[0]").value(this.messages("proexp.company.cannot.be.null.or.empty")));

    }

    @Test
    @DatabaseSetup(value = "proexp/proexp_controller_before_with_id.xml")
    @ExpectedDatabase(value = "proexp/proexp_controller_after_with_id.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void updateProExpWhereIdIs1() throws Exception{

        //Arrange
        ProfessionalExperience proExp = new ProfessionalExperience();
        proExp.setId(2);
        proExp.setCompany("Floricultura");
        proExp.setActivities("Gerente");
        proExp.setPerson(person);

        //Act and Assert
        this.mvc().perform(put("/professionalexperience")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.jsonWriter.writeValueAsString(proExp)))
                .andExpect(status().isOk())
                .andDo(print());

    }

    @Test
    public void updateProExpShouldReturn422WhenActivitiesIsNull() throws Exception{

        //Arrange
        ProfessionalExperience proExp = new ProfessionalExperience();
        proExp.setId(2);
        proExp.setCompany("Floricultura");
        proExp.setActivities(null);
        proExp.setPerson(person);

        //Act and Assert
        this.mvc().perform(post("/professionalexperience")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.jsonWriter.writeValueAsString(proExp)))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$[0]").value(this.messages("proexp.activities.cannot.be.null.or.empty")));

    }

    @Test
    @DatabaseSetup(value = "proexp/proexp_controller_before_with_id.xml")
    @ExpectedDatabase(value = "proexp/proexp_controller_delete_after_with_id.xml", assertionMode = DatabaseAssertionMode.NON_STRICT)
    public void deleteProExpWhereIdIs1() throws Exception{

        //Arrange
        ProfessionalExperience proExp = new ProfessionalExperience();
        proExp.setId(2);
        proExp.setCompany("Floricultura");
        proExp.setActivities("Operador de caixa");

        //Act and Assert
        this.mvc().perform(delete("/professionalexperience")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.jsonWriter.writeValueAsString(proExp)))
                .andExpect(status().isOk())
                .andDo(print());

    }

    @Test
    public void deleteProExpShouldReturnExceptionWhenIdDoesNotExists() throws Exception{

        //Arrange
        ProfessionalExperience proExp = new ProfessionalExperience();
        proExp.setId(999);
        proExp.setCompany("Floricultura");
        proExp.setActivities("Gerente");
        proExp.setPerson(person);

        //Act and Assert
        this.mvc().perform(delete("/professionalexperience")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.jsonWriter.writeValueAsString(person)))
                .andDo(print())
                .andExpect(status().isBadRequest());

    }

    @Test
    @DatabaseSetup(value = "proexp/proexp_controller_before_with_id.xml")
    public void getProExpByIdWhereIdIs2() throws Exception{

        //Act and Assert
        this.mvc().perform(get("/professionalexperience/{id}", "2")
                .param("id", "2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value("2"))
                .andExpect(jsonPath("company").value("Floricultura"))
                .andExpect(jsonPath("activities").value("Operador de caixa"));

    }

    @Test
    public void getProExpGetShouldReturnExceptionWhenIdIsNull() throws Exception{


        //Act and Assert
        this.mvc().perform(get("/professionalexperience/{id}", "")
                .param("id", ""))
                .andDo(print())
                .andExpect(status().is4xxClientError());

    }

}
