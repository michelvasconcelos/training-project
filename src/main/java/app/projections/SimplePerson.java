package app.projections;

public interface SimplePerson {

    long getId();
    String getName();
    String getEmail();

}
