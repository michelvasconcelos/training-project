package app.projections;


import java.util.List;

public interface CompletePerson {

    long getId();
    String getName();
    String getEmail();
    String getBirthDate();
    List<SimpleProfessionalExperience> getProfessionalExperienceList();

}
