package app.projections;

public interface SimpleProfessionalExperience {

    long getId();
    String getCompany();
    String getActivities();

}
