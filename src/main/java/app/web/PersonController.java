package app.web;

import app.domain.Person;
import app.exception.CouldNotFindEntityException;
import app.projections.CompletePerson;
import app.projections.SimplePerson;
import app.service.PersonService;
import app.service.ProfessionalExperienceService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Data
@RestController
@RequestMapping("/person")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PersonController {

    private final PersonService service;
    private final ProfessionalExperienceService professionalExperienceService;
    private final SpelAwareProxyProjectionFactory projectionFactory;

    @RequestMapping(method = POST)
    public void createPerson(@Valid @RequestBody Person person){

        Pattern pattern = Pattern.compile("T");
        String p = pattern.toString();

        person.setBirthDate(person.getBirthDate().split(p)[0]);

        service.save(person);

    }

    @RequestMapping(value = "/name/{name}", method = GET)
    public List<SimplePerson> getPersonByName(@PathVariable("name") String name){

       List<SimplePerson> people = new ArrayList<>();

       this.service.findByName(name)
                .forEach(p -> people.add(this.projectionFactory.createProjection(SimplePerson.class, p)));

       return people;

    }

    @RequestMapping(value = "/id/{id}", method = GET)
    public CompletePerson getPersonById(@PathVariable("id") long id){

        Person person = this.service.findById(id);

        //cria a projecao para pessoa e a retorna
        return  this.projectionFactory
                .createProjection(CompletePerson.class, person);
    }

    @RequestMapping(method = PUT)
    public void updatePerson(@Valid @RequestBody Person person){

        service.save(person);

    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public void deletePerson(@PathVariable("id") long id) throws Exception {

        Person person = service.findById(id);

        if(person == null)
            throw new CouldNotFindEntityException();
        else
            for(int i = 0; i < person.getProfessionalExperienceList().size(); i++){
                professionalExperienceService.delete(person.getProfessionalExperienceList().get(i));
            }
            person.getProfessionalExperienceList().clear();
            service.delete(person);

    }

}
