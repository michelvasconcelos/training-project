package app.web;

import app.domain.ProfessionalExperience;
import app.exception.CouldNotFindEntityException;
import app.projections.SimpleProfessionalExperience;
import app.service.ProfessionalExperienceService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping("/professionalexperience")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProfessionalExperienceController {

    @Autowired
    private ProfessionalExperienceService service;
    private final SpelAwareProxyProjectionFactory projectionFactory;

    @RequestMapping(method = POST)
    public void createProExp(@RequestBody ProfessionalExperience proExp){

        service.save(proExp);

    }

    @RequestMapping(value = "/{id}", method = GET)
    public SimpleProfessionalExperience readProExp(@PathVariable("id") long id){

        ProfessionalExperience proExp = service.findById(id);

        return this.projectionFactory.createProjection(SimpleProfessionalExperience.class, proExp);

    }

    @RequestMapping(method = PUT)
    public void updateProExp(@RequestBody ProfessionalExperience proExp){

        service.save(proExp);

    }

    @RequestMapping(method = DELETE)
    public void deleteProExp(@PathVariable("id") long id) throws CouldNotFindEntityException{

        ProfessionalExperience proExp = service.findById(id);

        if(proExp == null)
            throw new CouldNotFindEntityException();
        else
            service.delete(proExp);

    }

}
