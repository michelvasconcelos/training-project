package app.service;

import app.domain.Person;
import app.domain.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PersonService {

    private final PersonRepository repository;

    public void save(Person person){

        repository.save(person);
    }

    public List<Person> findByName(String nome){

        return (List<Person>) repository.findByNameContaining(nome);
    }

    public Person findById(long id){

        return repository.findOne(id);
    }

    public void delete(Person person){

        repository.delete(person);

    }

}
