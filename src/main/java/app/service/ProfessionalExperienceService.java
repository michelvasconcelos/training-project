package app.service;

import app.domain.ProfessionalExperience;
import app.domain.ProfessionalExperienceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfessionalExperienceService {

    @Autowired
    private ProfessionalExperienceRepository repository;

    public ProfessionalExperience findById(long id){

        return repository.findById(id);

    }

    public void save(ProfessionalExperience proExp){

        repository.save(proExp);
    }

    public void delete(ProfessionalExperience proExp){

        repository.delete(proExp);

    }

}
