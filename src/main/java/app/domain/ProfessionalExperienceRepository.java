package app.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessionalExperienceRepository extends CrudRepository<ProfessionalExperience, Long> {

    ProfessionalExperience findById(long id);

}
