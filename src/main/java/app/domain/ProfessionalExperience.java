package app.domain;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Getter @Setter
@Table(name = "EXP_PROFISSIONAL")
public class ProfessionalExperience {

    @Id
    @GeneratedValue
    private long id;

    @NotBlank(message = "proexp.company.cannot.be.null.or.empty")
    @Column(name = "EMPRESA")
    private String company;

    @NotBlank(message = "proexp.activities.cannot.be.null.or.empty")
    @Column(name = "ATIVIDADES")
    private String activities;

    @ManyToOne
    @JoinColumn(name = "ID_PESSOA")
    private Person person;

}