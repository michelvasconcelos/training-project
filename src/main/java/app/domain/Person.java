package app.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Entity
@Getter @Setter
@Table(name = "PESSOA")
public class Person{

    @Id
    @GeneratedValue
    private long id;

    @Column(name = "NOME")
    @NotBlank(message = "person.name.cannot.be.null.or.empty")
    private String name;

    @Column(name = "DATA_NASCIMENTO")
    private String birthDate;

    @Email(message = "person.email.cannot.be.malformed")
    private String email;

    @OneToMany(mappedBy = "person")
    private List<ProfessionalExperience> professionalExperienceList;

}
