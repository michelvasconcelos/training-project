app.controller("searchController", function ($scope, $location){

    $scope.searchForm = "";

    $scope.processSearch = function () {

        $location.path("/search/" + $scope.searchForm)

    }
});