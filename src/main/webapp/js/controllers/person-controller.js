app.controller("personController", function ($scope, $http, $routeParams, $location) {

    $scope.person = {};
    $scope.personUpdate = {};
    $scope.proExp = {};
    $scope.proExpUpdate = {};

    $scope.formUpdatePerson = function () {
        $('#person').addClass('hide');
        $('#formUpdatePerson').removeClass('hide');
        $scope.personUpdate.id = $scope.person.id;
        $scope.personUpdate.name = $scope.person.name;
        $scope.personUpdate.email = $scope.person.email;
        $scope.personUpdate.birthDate = $scope.person.birthDate;
        $scope.personUpdate.professionalExperienceList = $scope.person.professionalExperienceList;

    };

    $scope.cancelUpdatePerson = function () {
        $('#formUpdatePerson').addClass('hide');
        $('#person').removeClass('hide');
    };

    $scope.addProExp = function () {
        $('#proExpForm').removeClass('hide');
    };

    $scope.cancelAddProExp = function () {
        $('#proExpForm').addClass('hide');
    };

    $scope.updateProExp = function (p) {
        $('#updateProExpForm'+p.id).removeClass('hide');
        $('#proExp'+p.id).addClass('hide');
        $scope.proExpUpdate.company = p.company;
        $scope.proExpUpdate.activities = p.activities;
        $scope.proExpUpdate.id = p.id;
    };

    $scope.cancelUpdateProExp = function (p) {
        $('#updateProExpForm'+p.id).addClass('hide');
        $('#proExp'+p.id).removeClass('hide');
    };

    $http({

        method  :   'GET',
        url     :   'http://localhost:8080/person/id/' + $routeParams.id

    })
    .then(function successCallback(response) {
        // this callback will be called asynchronously
        // when the response is available

        console.log(response);
        $scope.person = response.data;

    }, function errorCallback(response) {

        // called asynchronously if an error occurs
        // or server returns response with an error status.

        console.log(response);

    });

    $scope.updatePerson = function () {

        if($scope.personUpdate.name !== null){

            $http({

                method  :   'POST',
                url     :   'http://localhost:8080/person',
                data    :   $scope.personUpdate

            })
            .then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available

                console.log(response);
                alert("Pessoa atualizada com sucesso!");

                // if update person in scope
                $scope.message = response.message;
                $scope.person.name = $scope.personUpdate.name;
                $scope.person.birthDate = $scope.personUpdate.birthDate;
                $scope.cancelUpdatePerson();

            }, function errorCallback(response) {

                // called asynchronously if an error occurs
                // or server returns response with an error status.

                console.log(response);
                alert("Falha ao atualizar pessoa");

            });

        }else{
            alert("Valores invalidos")
        }

    };

    $scope.deletePerson = function (p) {

        $http({

            method  :   'DELETE',
            url     :   'http://localhost:8080/person/' + p.id

        })
        .then(function successCallback(response) {
            // this callback will be called asynchronously
            // when the response is available

            console.log(response);
            alert("Pessoa removida com sucesso!");
            $location.path("/");


        }, function errorCallback(response) {

            // called asynchronously if an error occurs
            // or server returns response with an error status.

            console.log(person);
            console.log(response);
            alert("Falha ao remover pessoa");

        });

    };

    $scope.createProfessionalExperience = function () {

        if(($scope.proExp.company !== null && $scope.proExp.company !== "") && ($scope.proExp.activities !== null && $scope.proExp.activities !== "")){

            $scope.proExp.person = $scope.person;

            $http({

                method  :   'POST',
                url     :   'http://localhost:8080/professionalexperience',
                data    :   $scope.proExp

            })
                .then(function successCallback(response) {
                    // this callback will be called asynchronously
                    // when the response is available

                    console.log(response);
                    alert("Experiencia profissional adicionada com sucesso");
                    $scope.cancelAddProExp();
                    $scope.person.professionalExperienceList.push($scope.proExp);
                    $scope.proExp = {};


                }, function errorCallback(response) {

                    // called asynchronously if an error occurs
                    // or server returns response with an error status.

                    console.log(person);
                    console.log(response);
                    alert("Falha ao adicionar experiencia profissional");

                });

        }else{
            alert("Dados incompletos")
        }
    };

    $scope.updateProfessionalExperience = function (p) {

        if(($scope.proExpUpdate.company !== null && $scope.proExpUpdate.company !== "") && ($scope.proExpUpdate.activities !== null && $scope.proExpUpdate.activities !== "")){

            var pos = $scope.person.professionalExperienceList.indexOf(p);
            $scope.proExpUpdate.person = $scope.person;

            $http({

                method  :   'PUT',
                url     :   'http://localhost:8080/professionalexperience',
                data    :   $scope.proExpUpdate

            })
                .then(function successCallback(response) {
                    // this callback will be called asynchronously
                    // when the response is available

                    console.log(response);
                    alert("Experiencia profissional alterada com sucesso");
                    delete $scope.proExpUpdate.person;
                    $scope.person.professionalExperienceList[pos].id = $scope.proExpUpdate.id;
                    $scope.person.professionalExperienceList[pos].company = $scope.proExpUpdate.company;
                    $scope.person.professionalExperienceList[pos].activities = $scope.proExpUpdate.activities;
                    $scope.cancelUpdateProExp($scope.person.professionalExperienceList[pos]);
                    $scope.proExpUpdate = {};

                }, function errorCallback(response) {

                    // called asynchronously if an error occurs
                    // or server returns response with an error status.

                    console.log(person);
                    console.log(response);
                    alert("Falha ao alterar experiencia profissional");

                });

        }else{
            alert("Dados incompletos")
        }

    };


    $scope.deleteProfessionalExperience = function (p) {

        $http({

            method  :   'DELETE',
            url     :   'http://localhost:8080/professionalexperience/' + p.id

        })
        .then(function successCallback(response) {
            // this callback will be called asynchronously
            // when the response is available

            console.log(response);
            alert("Experiencia profissional excluida com sucesso");
            delete $scope.person.professionalExperienceList[$scope.person.professionalExperienceList.indexOf(p)];

        }, function errorCallback(response) {

            // called asynchronously if an error occurs
            // or server returns response with an error status.

            console.log(person);
            console.log(response);
            alert("Falha ao excluir experiencia profissional");

        });
    }

});