app.controller("signupPersonController", function ($scope, $http) {

    // create a blank object to hold our form information
    // $scope will allow this to pass between controller and view

    $scope.formPerson = {};

    // process the form
    $scope.processFormPerson = function () {
        console.log($scope.formPerson);

        $http({

            method  :   'POST',
            url     :   'http://localhost:8080/person',
            data    :   $scope.formPerson

        })
        .then(function successCallback(response) {
            // this callback will be called asynchronously
            // when the response is available

            console.log($scope.formPerson);
            console.log(response);
            alert("Pessoa inserida com sucesso!");
            $scope.formPerson.name = "";
            $scope.formPerson.email = "";
            $scope.formPerson.birthDate = "";

            // if successful, bind success message to message
            $scope.message = response.message;

        }, function errorCallback(response) {

            // called asynchronously if an error occurs
            // or server returns response with an error status.

            console.log(response);
            alert("Falha ao inserir pessoa");
            $scope.formPerson.name = "";
            $scope.formPerson.email = "";
            $scope.formPerson.birthDate = "";

        });
    };
});