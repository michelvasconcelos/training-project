app.controller("disambiguationController", function ($scope, $http, $routeParams){

    $scope.people = [];


    $http({

        method  :   'GET',
        url     :   'http://localhost:8080/person/name/' + $routeParams.name

    })
    .then(function successCallback(response) {
        // this callback will be called asynchronously
        // when the response is available

        console.log(response);
        response.data.forEach(function (person) {
            $scope.people.push(person);
        })


    }, function errorCallback(response) {

        // called asynchronously if an error occurs
        // or server returns response with an error status.

        console.log(response);

    });


});