//modulo principal da aplicacao
var app = angular.module('app', ['ngRoute']);

app.config(function ($routeProvider, $locationProvider) {

    $routeProvider
        .when("/", {

            templateUrl:'views/signup-person.html',
            controller: 'signupPersonController'

        })
        .when("/search/:name", {

            templateUrl:'views/disambiguation.html',
            controller: 'disambiguationController'

        })
        .when("/person/:id", {

            templateUrl: 'views/person.html',
            controller: 'personController'

        });

    $locationProvider.html5Mode(true);

});