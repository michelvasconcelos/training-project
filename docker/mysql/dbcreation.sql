create table EXP_PROFISSIONAL
(
	ID int auto_increment
		primary key,
	EMPRESA varchar(255) not null,
	ATIVIDADES text not null,
	ID_PESSOA int null,
	constraint EXP_PROFISSIONAL_ibfk_1
		foreign key (ID_PESSOA) references `TRAINING-PROJECT-DB`.PESSOA (ID)
)
;

create index ID_PESSOA
	on EXP_PROFISSIONAL (ID_PESSOA)
;

create table PESSOA
(
	ID int auto_increment
		primary key,
	NOME varchar(255) not null,
	DATA_NASCIMENTO date null,
	EMAIL varchar(255) null
)
;

